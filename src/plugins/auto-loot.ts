(() => {
    const id = 'auto-loot';
    const title = 'AutoLoot';
    const desc = 'Self-splanatory. :)';
    const imgSrc = 'assets/media/main/bank_header.svg';
    SEMI.add(id, {
        ms: 15000,
        imgSrc,
        onLoop: () => {
            // @ts-ignore-line
            if (player.manager.loot.drops.length !== 5) player.manager.loot.lootAll();
        },
        desc,
        title,
        pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT,
    });
})();
