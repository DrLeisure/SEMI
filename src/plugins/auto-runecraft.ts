(() => {
    const id = 'auto-runecraft';
    const title = 'AutoRunecraft';
    const desc = 'AutoRunecraft will runecraft runes based on the rune use ratios you select.';

    const imgSrc = 'assets/media/bank/rune_chaos.png';
    const skill = 'Runecrafting';

    const menuConfig = {};
    const defaultRatioConfig = {};

    const MODE_LINEAR = 1;
    const MODE_LOWEST_CAST = 2;
    const MODE_ENDLESS = 3;

    // debug
    const debugLog = (...msg) => {
        console.log(`[${title}]`, ...msg);
    };

    // generate rune options
    const RUNE_DATA = {};
    const RUNE_DATA_ORDER = [];
    runecraftingItems.forEach((item, index) => {
        let runeItem = items[item.itemID];
        if (runeItem.type == 'Rune' && runeItem.runecraftingID != null) {
            // && runeItem.runecraftingCategory == 0) {
            RUNE_DATA[item.itemID] = {
                action: index,
                category: runeItem.runecraftingCategory,
                itemID: item.itemID,
                item: runeItem,
                recipe: item,
            };
            RUNE_DATA_ORDER.push(RUNE_DATA[item.itemID]);
            defaultRatioConfig[runeItem.itemID] = 0;
        }
    });
    RUNE_DATA_ORDER.sort((a, b) => {
        return a.action + a.category * 1000 > b.action + b.category * 1000 ? 1 : -1;
    });

    // config
    const USE_HOOK_SWITCH = true; // kinda cheaty but prevents any time loss from switching which rune to craft

    const scriptState = {};

    const getConfig = () => {
        let storedMenuConfig = SEMI.getItem(`${id}-config`);
        if (storedMenuConfig !== null) {
            // In the event a rune is add / removed, make sure the ratios have a key default.
            Object.keys(defaultRatioConfig)
                .filter((itemID) => {
                    return storedMenuConfig.runeRatio[itemID] == null;
                })
                .forEach((itemID) => {
                    storedMenuConfig.runeRatio[itemID] = 0;
                });

            return storedMenuConfig;
        }

        return {
            runeRatio: defaultRatioConfig,
            craftingMode: MODE_ENDLESS,
            baseCastCount: 10000,
            baseRestockCount: 500,
        };
    };

    const storeConfig = () => {
        SEMI.setItem(`${id}-config`, menuConfig['ARC']);
    };

    const resetScriptState = () => {
        scriptState.restockEssence = false;
        scriptState.restockRune = null;
        scriptState.switchToRune = null;
    };

    const onEnable = () => {
        resetScriptState();
        injectAutoRunecraftGUI();
    };

    const onDisable = () => {
        resetScriptState();
        SEMIUtils.stopSkill(skill);
        removeAutoRunecraftGUI();
    };

    // ui
    const injectAutoRunecraftGUI = () => {
        if ($(`#${id}`).length) {
            return;
        }

        // load config
        menuConfig['ARC'] = getConfig();

        const createRuneButton = (item) => {
            return `
            <div id="arc-btn-${item.itemID}" class="col-sm-6 col-md-3 col-xl-2">
                <div class="block block-rounded-double bg-combat-inner-dark pl-2 pr-2 pt-1 pb-2 border-top ${
                    item.category == 0 ? 'border-smithing' : 'border-crafting'
                } border-2x">
                    <div class="row row-deck gutters-tiny">
                        <div class="col-12"><small class="font-w700 text-left text-combat-smoke m-1"><img src="${getItemMedia(
                            item.itemID
                        )}" class="skill-icon-xs mr-1"> ${item.item.name}</small></div>
                        <div class="col-12"><input type="text" class="form-control form-control-small" id="arc-ratio-${
                            item.itemID
                        }" placeholder="0"></input></div>
                        <div class="col-12 small pt-1" style="text-align:center;">
                            <div class="border-right border-smithing border-1x" style="width: 50%;display:inline-block;">
                                Quantity:<br><span id="arc-view-have-${item.itemID}">${numberWithCommas(
                runeCount(item.itemID)
            )}</span>
                            </div>
                            <div style="width: 50%;display:inline-block;">
                                Cast Possible:<br><span id="arc-view-cast-${item.itemID}">${runesCastFormat(
                item.itemID
            )}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;
        };

        const autoRunecraftDiv = `
                <div id="${id}" class="col-12">
                    <div class="block block-rounded block-link-pop border-top border-mining border-4x" style="padding-bottom: 12px; display: flex; flex-direction: column">
                        <div class="block-header text-center"><h3 class="block-title">AutoRunecraft Rune Selection</h3></div>
                        <div class="block-content text-center font-w400 font-size-sm" style="padding-top: 4px;">
                            Craft by ratios of your selection. Choose the base spell cast count you wish to craft for, then calculate your rune use for each cast and put it into the rune boxes. Will begin mining rune essence if you run out. Combination Runes will craft 500 of the require ingreadients if it runs out to contine crafting them.
                        </div>
                        <div class="block-content" style="padding-top: 12px; margin-top: auto;">
                            <div class="row row-deck gutters-tiny">
                                <div class="col-sm-6 col-md-4">
                                    <div class="block block-rounded-double bg-combat-inner-dark p-3">
                                        <div class="border-bottom border-mining border-2x pb-2">Crafting Mode:</div>
                                        <div class="form-group pt-2 mb-0">
                                            <div id="arc-cmd-1" class="custom-control custom-radio custom-control-inline custom-control-lg">
                                                <input type="radio" class="custom-control-input" id="arc-crafting-mode-1" name="arc-craft-mode" value="1">
                                                <label class="custom-control-label" for="arc-crafting-mode-1">Linear</label>
                                            </div>
                                            <div id="arc-cmd-2" class="custom-control custom-radio custom-control-inline custom-control-lg">
                                                <input type="radio" class="custom-control-input" id="arc-crafting-mode-2" name="arc-craft-mode" value="2">
                                                <label class="custom-control-label" for="arc-crafting-mode-2">Lowest Cast</label>
                                            </div>
                                            <div id="arc-cmd-3" class="custom-control custom-radio custom-control-inline custom-control-lg">
                                                <input type="radio" class="custom-control-input" id="arc-crafting-mode-3" name="arc-craft-mode" value="3">
                                                <label class="custom-control-label" for="arc-crafting-mode-3">Endless Crafting</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="block block-rounded-double bg-combat-inner-dark p-3">
                                        <div class="border-bottom border-mining border-2x pb-2">Max Cast: <small>[Linear / Lowest Cast]</small></div>
                                        <div class="form-group pt-2 mb-0">
                                            <input type="text" class="form-control form-control-small" id="arc-cast-count" placeholder="10000"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="block block-rounded-double bg-combat-inner-dark p-3">
                                        <div class="border-bottom border-mining border-2x pb-2">Rune Essence Restock Count:</div>
                                        <div class="form-group pt-2 mb-0">
                                            <input type="text" class="form-control form-control-small" id="arc-restock-count" placeholder="1000"></input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-deck gutters-tiny">
                                ${RUNE_DATA_ORDER.map((runeItem, index) => createRuneButton(runeItem)).join('')}
                            </div>
                        </div>
                    </div>
                </div>`;

        const inputDefaultFill = () => {
            $(`#arc-crafting-mode-${menuConfig['ARC'].craftingMode}`).prop('checked', true);
            $('#arc-cast-count').val(menuConfig['ARC'].baseCastCount);
            $('#arc-restock-count').val(menuConfig['ARC'].baseRestockCount);

            $.each(menuConfig['ARC'].runeRatio, function (index, value) {
                $(`#arc-ratio-${index}:text`).val(value);
            });
        };

        const inputEventTriggers = () => {
            $('[name=arc-craft-mode]').on('click', function () {
                menuConfig['ARC'].craftingMode = parseInt($(this).val());
                storeConfig();
            });
            $.each(RUNE_DATA, function (index, value) {
                $('#arc-ratio-' + index).on('input', function () {
                    menuConfig['ARC'].runeRatio[index] = parseInt($(this).val());
                    updateRuneCounts(value.itemID);
                    storeConfig();
                });
            });
            $('#arc-cast-count').on('input', function () {
                menuConfig['ARC'].baseCastCount = parseInt($(this).val());
                storeConfig();
            });
            $('#arc-restock-count').on('input', function () {
                menuConfig['ARC'].baseRestockCount = parseInt($(this).val());
                storeConfig();
            });
        };

        const inputAddTippy = () => {
            [
                'Crafts each rune until the Max Cast setting is met before switching.</div>',
                'Crafts each rune until the Max Cast setting is met, switching to the rune that has the lowest amount of cast currently possible.',
                'Crafts runes endlessly following lowest cast amount.',
            ].forEach((desc, i) => {
                tippy(`#arc-cmd-${i + 1}`, {
                    content: `<div class="text-center">${desc}</div>`,
                    placement: 'top',
                    allowHTML: true,
                    interactive: false,
                    animation: false,
                });
            });
        };

        $('#runecrafting-container .row .col-12:first').after($(autoRunecraftDiv));

        inputDefaultFill();
        inputEventTriggers();
        inputAddTippy();
    };

    const removeAutoRunecraftGUI = () => {
        $(`#${id}`).remove();
    };

    const updateRuneCounts = (itemID) => {
        $(`#arc-view-have-${itemID}`).text(numberWithCommas(runeCount(itemID)));
        $(`#arc-view-cast-${itemID}`).text(runesCastFormat(itemID));
    };

    // hook
    // const _updateRunecraftQty = updateRunecrafting;
    // updateRunecraftQty = (item) => {
    //     _updateRunecrafting(item);

    //     // Prevent time loss on switching
    //     if (scriptState.switchToRune != null) {
    //         if (getSelectedRuneID() != scriptState.switchToRune.itemID) {
    //             runecraftingCategory(scriptState.switchToRune.category);
    //             selectRunecraft(scriptState.switchToRune.action);
    //         }
    //         scriptState.switchToRune = null;
    //     }
    // };

    // script
    const runeCount = (itemID) => {
        return SEMIUtils.getBankQty(itemID);
    };

    const runesCast = (itemID) => {
        return menuConfig['ARC'].runeRatio[itemID] > 0
            ? Math.floor(runeCount(itemID) / menuConfig['ARC'].runeRatio[itemID])
            : -1;
    };

    const runesCastFormat = (itemID) => {
        let castAmount = runesCast(itemID);
        return castAmount < 0 ? '-' : numberWithCommas(castAmount);
    };

    const getSelectedRuneID = () => {
        return isRunecrafting ? runecraftingItems[selectedRunecraft].itemID : null;
    };

    const beginScriptCrafting = (runeItem) => {
        runecraftingCategory(runeItem.category);
        selectRunecraft(runeItem.action);
        startRunecrafting(true);
    };

    const autoRunecraft = () => {
        // Assure the counts are correct before each loop
        for (const runeItem of RUNE_DATA_ORDER) {
            updateRuneCounts(runeItem.itemID);
        }

        // Check if we even have enough rune essence
        let runeEssenceCount = runeCount(CONSTANTS.item.Rune_Essence);
        if (runeEssenceCount === 0) {
            scriptState.restockEssence = true;
            debugLog('Begun Restocking Rune Essence...');
        }

        // Restocking Rune Essence
        if (scriptState.restockEssence) {
            let runeEssRestockMin = Math.max(1, menuConfig['ARC'].baseRestockCount);

            debugLog(`...Waiting on Rune Essence [${runeEssenceCount} / ${runeEssRestockMin}]...`);

            if (runeEssenceCount >= runeEssRestockMin) {
                scriptState.restockEssence = false;
                debugLog('Restocking Rune Essence Finished');
                return;
            } else if (game.mining.selectedRockId != 10) {
                game.mining.onRockClick(10);
                debugLog('Begin Mining Rune Essence');
                return;
            }
            return;
        }

        let limitedCrafting =
            menuConfig['ARC'].craftingMode == MODE_LINEAR || menuConfig['ARC'].craftingMode == MODE_LOWEST_CAST;
        let currentRune = getSelectedRuneID();

        // Currently crafting a rune linearly, wait till this rune is at the max cast setting
        if (menuConfig['ARC'].craftingMode == MODE_LINEAR && currentRune != null) {
            let runeCastable = runesCast(currentRune);
            if (runeCastable >= 0 && runeCastable < menuConfig['ARC'].baseCastCount) {
                debugLog(
                    `Crafting Linearly, nothing to change... [${runeCastable} / ${menuConfig['ARC'].baseCastCount}]`
                );
                return;
            }
        }

        // Crafting Runes for Combination Runes Ratio
        if (scriptState.restockRune != null) {
            // check we have enough to resume combination crafting
            let restockItemQty = runeCount(scriptState.restockRune.itemID);

            debugLog(`...Waiting on ${scriptState.restockRune.item.name} Restock [${restockItemQty} / 500]...`);

            if (restockItemQty > 500) {
                debugLog('Restocking Rune Finished');
                scriptState.restockRune = null;
                return;
            }

            // check we are crafting the required restock rune
            if (isRunecrafting && currentRune != scriptState.restockRune.itemID) {
                debugLog('Restocking Rune...');
                beginScriptCrafting(scriptState.restockRune);
                return;
            }
            return;
        }

        // Find lowest rune
        let lowestRune = null;
        let lowestCast = Number.POSITIVE_INFINITY;
        for (const runeItem of RUNE_DATA_ORDER) {
            if (skillLevel[CONSTANTS.skill.Runecrafting] >= runeItem.recipe.level) {
                // check runecrafting level just in case
                let runeCastable = runesCast(runeItem.itemID);

                if (runeCastable >= 0) {
                    // max crafting requirement reached, skipping
                    if (limitedCrafting && runeCastable >= menuConfig['ARC'].baseCastCount) {
                        continue; // reached max requested cast amounts
                    }

                    // new lowest to craft
                    if (runeCastable < lowestCast) {
                        lowestCast = runeCastable;
                        lowestRune = runeItem;
                    }

                    // found a craftable rune, continue
                    if (menuConfig['ARC'].craftingMode == MODE_LINEAR) {
                        break;
                    }
                }
            }
        }

        // Nothing to craft
        if (lowestRune == null) {
            if (game.mining.selectedRockId != 10) {
                debugLog('No rune to craft, mining Rune Essence...');
                game.mining.onRockClick(10);
            }
            return;
        }

        // Combination Runes
        let craftItemRequirements = lowestRune.item.runecraftReq;
        if (craftItemRequirements.length > 1) {
            debugLog(`Checking Combination Rune [${lowestRune.item.name}] Requirements`);
            for (let i = 1; i < craftItemRequirements.length; i++) {
                let craftRuneItem = craftItemRequirements[i];

                if (runeCount(craftRuneItem.id) < craftRuneItem.qty) {
                    debugLog('Begin Combination Rune Requirements...');
                    scriptState.restockRune = RUNE_DATA[craftRuneItem.id];
                    lowestRune = scriptState.restockRune;
                    break;
                }
            }
        }

        // All ready crafting the lowest rune
        if (lowestRune.itemID == currentRune) {
            debugLog(`Already Crafting Lowest Rune [${lowestRune.item.name}]`);
            return;
        }

        // Begin Crafting
        debugLog(`Begin Crafting Lowest Rune [${lowestRune.item.name}]...`);
        beginScriptCrafting(lowestRune);
    };

    SEMI.add(id, {
        ms: 5000,
        onLoop: autoRunecraft,
        onEnable,
        onDisable,
        title,
        desc,
        imgSrc,
        skill,
    });
})();
